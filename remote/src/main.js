let { app } = require('electron')

if(app.isPackaged){
	require('../dist/app/remote')
} else {
	console.log(`App not packaged, running dev`);
	require('@babel/register')
	require('./remote')
}
