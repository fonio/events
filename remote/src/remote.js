import { app } 		from 'electron'
import MainWindow 	from './app/MainWindow'

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = true;

app.on('ready', ()=>{
	new MainWindow()
})
