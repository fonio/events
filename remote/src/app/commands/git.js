import git 		from 'simple-git/promise'
import Command 	from './Command'
import { log } 	from '../../../../common/utils'

class Git extends Command {

	constructor(){
		super('git')
	}

	async version(){
		let version = await this.run(`${this.cmd} --version`)
		return /[0-9]+\.[0-9]+\.[0-9]+/.exec(version)[0]
	}	

	async clone(repositoryUrl, localPath){
		log(`Running git clone ${repositoryUrl} ${localPath}`)
		return await git().clone(repositoryUrl, localPath)
	}

}

export default new Git
