import os from 'os'
import Command 	from './Command'
import {
	MESSAGE,
	MESSAGE_COMMAND_DATA,
	MESSAGE_COMMAND_ERROR,
	MESSAGE_COMMAND_COMPLETE,
	MESSAGE_COMMAND_ERRORCOMPLETE
} 						from '../../../../common/constants'

class PackageManager extends Command {

	constructor(){
		switch(os.platform()){
			case 'darwin': 
				super(PM_BREW);
				break;

			case 'win32': 
				super(PM_CHOCO)
				break;

				default: 
					throw new Error(`Unknown os ${os.platform()}`)
		}
	}

	version(){
		return new Promise(async (resolve, reject)=>{
			try {
				let version = await this.run(`${this.cmd} --version`)
				resolve(/[0-9]+\.[0-9]+\.[0-9]+/.exec(version)[0])
			} catch(e){
				reject(e)
			}
		})
	}

	async install(){
		if(this.cmd === PM_BREW){
			return await this.run(`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`)
		} else {
			return await this.run(`powershell Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) && refreshenv`)
		}
	}

}

export const PM_BREW 	= 'brew'
export const PM_CHOCO 	= 'choco'

export default new PackageManager
