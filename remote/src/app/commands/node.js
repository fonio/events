import Command 	from './Command'
import path 	from 'path'

class NPM extends Command {

	constructor(){
		super('npm')
	}

	// not the standard command install, override and run 'npm install'
	async install(from){
		return await this.run(`cd ${path.normalize(from.replace(/ /g, '\\ '))} && ${this.cmd} install`)
	}

	async start(from){
		return await this.run(`cd ${path.normalize(from.replace(/ /g, '\\ '))} && ${this.cmd} start`)
	}
}

class Node extends Command {

	constructor(){
		super('node')
		this.npm = new NPM
	}

	async version(){
		let version = await this.run(`${this.cmd} --version`)
		return /[0-9]+\.[0-9]+\.[0-9]+/.exec(version)[0]
	}

}

export default new Node
