import { spawnSync, spawn } 	from 'child_process'
import os				from 'os'
import EventsEmitter 	from 'events'
import { log } 			from '../../../../common/utils'
import {
	MESSAGE_COMMAND,
	MESSAGE_COMMAND_DATA,
	MESSAGE_COMMAND_ERROR,
	MESSAGE_COMMAND_COMPLETE,
	MESSAGE_COMMAND_ERRORCOMPLETE
} 						from '../../../../common/constants'
import socket 			from '../ServiceSocket'

class Command extends EventsEmitter {

	constructor(cmd){
		super()
		this.cmd = cmd
	}

	run(cmdStr){
		return new Promise((resolve, reject)=>{
			log(`Running command ${cmdStr}`, { color: 'yellow' })

			let dataBuffer 	= [],
				errorBuffer = []

			this.cmdProcess = spawn(cmdStr, { shell: true })

			this.cmdProcess.stdout.on('data', data=>{
				dataBuffer.push(data)
				this.emit(MESSAGE_COMMAND_DATA, data.toString())
			})

			this.cmdProcess.stderr.on('data', data=>{
				errorBuffer.push(data)
				this.emit(MESSAGE_COMMAND_ERROR, data.toString())
			})

			this.cmdProcess.on('close', code=>{
				let str = `Command ${cmdStr} closed with code ${code}`,
					dataStr = Buffer.concat(dataBuffer).toString(),
					errorStr = Buffer.concat(errorBuffer).toString()

				socket.send(MESSAGE_COMMAND, str)
				log(str, { color: 'yellow' })

				this.emit(
					MESSAGE_COMMAND_COMPLETE,
					code,
					dataStr, 
					errorStr
				)

				if(code === 0){
					resolve(dataStr)
				} else {
					reject(errorStr)
				}
			});
		})
	}

	kill(){
		if(this.cmdProcess){
			this.cmdProcess.kill('SIGKILL')
		}
	}

} 

export default Command
