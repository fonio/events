import { ipcMain, app, Menu } 
					from 'electron'
import Remote 		from './Remote'
import View 		from './View'


import { spawn } 	from 'child_process'
import {
	MESSAGE_WINDOW_MESSAGE,
} 					from '../../../common/constants'


export default class MainWindow {

	constructor(){
		let view 		= new View(),
			remote 		= new Remote(view)

		remote.init()

		view.window.on('closed', ()=>{
			remote.close()
			app.quit()
		});

		app.on('before-quit', ()=>{
  			remote.close()
		});

		app.on('window-all-closed', function(){
			if(process.platform != 'darwin') app.quit();
		});

		// setup the menu
		const menu = Menu.buildFromTemplate([{
			label: app.name,
			submenu: [
				{
					label: 'Reset', 
					click() { 
						remote.reset()
					} 
				},
/*				{
					label: 'BENSTEST', 
					click() { 

		let cmdStr = `powershell Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) && refreshenv`,
			cmd = spawn(cmdStr, { shell: true })

		cmd.stdout.on('data', data=>{
			console.log(data);
			view.send(MESSAGE_WINDOW_MESSAGE, data)
		})
		cmd.stderr.on('data', data=>{
			console.log(`error: ${data}`);
			view.send(MESSAGE_WINDOW_MESSAGE, `error: ${data}`)
		})

		cmd.on('close', code=>{
			let str = `Command ${cmdStr} closed with code ${code}`

			view.send(MESSAGE_WINDOW_MESSAGE, str)
		});

					} 
				},	*/
				{
					role: 'togglefullscreen',
					accelerator: 'CmdOrCtrl+Shift+F'
				},				
				{ role: 'toggledevtools' },
				{ type: 'separator' },
				{ role: 'quit' }
			]}
		])

		Menu.setApplicationMenu(menu)	

	}

}
