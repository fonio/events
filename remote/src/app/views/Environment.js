import { app } 				from 'electron'
import fs 					from 'fs'
import del 					from 'del'
import os 					from 'os'
import path 				from 'path'
import { execFile }			from 'child_process'
import semver 				from 'semver'
import node 				from '../commands/node'
import git 					from '../commands/git'
import loadView 			from '../decorators/loadView'
import socket 				from '../ServiceSocket'
import Store 				from '../Store'
import Project 				from './Project'
import {
	STORE_ENVIRONMENT_SETTINGS,
	VERSION_NODE, 
	VERSION_GIT,
	MESSAGE_WINDOW_MESSAGE,
	VERSION_PM,
	MESSAGE,
	MESSAGE_COMMAND_DATA,
	MESSAGE_COMMAND_ERROR,
	MESSAGE_COMMAND_COMPLETE
} 							from '../../../../common/constants'
import { log }				from '../../../../common/utils'

@loadView
export default class Environment {

	get name(){
		return 'environment'
	}

	constructor(view, socket){
		this.view = view
		this.socket = socket
	}

	async setup(settings){

		try {
			let gitVersion = await git.version()
			this.view.send(MESSAGE_WINDOW_MESSAGE, `git is installed (version ${gitVersion})`)

			let nodeVersion = await node.version()
			this.view.send(MESSAGE_WINDOW_MESSAGE, `node is installed (version ${nodeVersion})`)

		} catch(e){
			log(e);
			await this.install()
		}

		try {
			this.view.send(MESSAGE_WINDOW_MESSAGE, `Cloning ${settings.repository_url} to ${Project.projectPath}`)
			await del(Project.projectPath, { force: true })

			await git.clone(settings.repository_url, Project.projectPath)
			this.view.send(MESSAGE_WINDOW_MESSAGE, `Running npm install...`)

			await node.npm.install(Project.projectPath)

			return Promise.resolve()
		} catch(e){
			this.view.send(MESSAGE_WINDOW_MESSAGE, `Setup error: ${e}`)
		}
	}

	async install(){
		return new Promise(async (resolve, reject)=>{
			this.view.send(MESSAGE_WINDOW_MESSAGE, `Running install script...`)

			let scriptName 
			switch(os.platform()){
				case 'darwin': scriptName = 'mac.sh'
					break;
				case 'win32': scriptName = 'windows.bat' 
					break;
				default: 
					throw new Error(`Environment install: Unknown os ${os.platform()}`)
			}

			let pathDetails = app.isPackaged ? [process.resourcesPath, 'app.asar.unpacked', 'remote', 'src', 'app'] : [__dirname, '..']
			let scriptPath = path.join(...pathDetails, 'commands', 'install', scriptName)
			this.view.send(MESSAGE_WINDOW_MESSAGE, scriptPath)

			let scriptStr = fs.readFileSync(scriptPath).toString()

			this.view.send(MESSAGE_WINDOW_MESSAGE, scriptStr)

			let script = execFile(scriptPath, (err, stdout, stderr)=>{
				if(err){
					this.view.send(MESSAGE_WINDOW_MESSAGE, err)
					return reject(err)
				}

				this.view.send(MESSAGE_WINDOW_MESSAGE, stdout)
				resolve(stdout)
			})
		})
	}

}
