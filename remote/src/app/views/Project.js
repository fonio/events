import { app } 	from 'electron'
import url 		from 'url'
import request 	from 'request-promise-native'
import fs 		from 'fs'
import del 		from 'del'
import semver 	from 'semver'
import Store 	from '../Store'
import git 		from '../commands/git'
import node 	from '../commands/node'
import loadView from '../decorators/loadView'
import {
	VERSION_NODE,
	STORE_PROJECT_SETTINGS,
	STORE_ENVIRONMENT_SETTINGS,
	MESSAGE_COMMAND_COMPLETE
} 				from '../../../../common/constants'
import { 
	ProjectSettingsError,
	EnvironmentSettingsError,
	ProjectSetupError
} 				from '../../../../common/errors'

@loadView
class Project {

	get name(){
		return 'project'
	}

	get settings(){
		return Store.get(STORE_PROJECT_SETTINGS)
	}

	set settings(settings){
		Store.set(STORE_PROJECT_SETTINGS, settings)
	}

	static get projectPath(){
		return `${app.getPath('userData')}/project`
	}

	constructor(view){
		this.view 			= view
		this.loadViewDelay 	= true
	}

	start(){
		// are settings present?
		if(!this.settings){
			throw new ProjectSettingsError
		}

		// does package.json exist?
		if(!fs.existsSync(`${Project.projectPath}/package.json`)){
			throw new ProjectSetupError
		}

		// all tests passed, start up
		node.npm.start(Project.projectPath)

		if(this.settings.launch_url){
			this.loadLaunchURL()
		} else {
			this.loadViewDelay = false
			this.loadView()
		}
	}

	stop(){
		node.npm.kill()
	}

	async reset(){
		this.stop()
		this.settings = null
		return await del(Project.projectPath, { force: true })
	}

	async loadLaunchURL(){
		try {
			// test to see if project node process has started
			await request(this.settings.launch_url)
		} catch(e){
			// process has not yet started, attempt again
			setTimeout(this.loadLaunchURL.bind(this), 250)
		}
		
		this.view.window.loadURL(url.format(this.settings.launch_url))
	}

}

export default Project
