import EventsEmitter 		from 'events'
import loadView 			from '../decorators/loadView'
import socket 				from '../ServiceSocket'
import {
	MESSAGE_SERVICE_STATUS,
	MESSAGE_REGISTRATION_ERROR,
	MESSAGE_REGISTRATION_REGISTER,
	MESSAGE_REGISTRATION_REGISTERED
} 							from '../../../../common/constants'

@loadView
export default class Registration {

	/**
	 * Name provides loadView with html filename to load
	 * @return {String} [description]
	 */
	get name(){
		return 'registration'
	}

	constructor(view){
		this.view 		= view
		this.emitter 	= new EventsEmitter
	}

	onViewReady(){
		// send the initial isConnected flag
		this.view.send(MESSAGE_SERVICE_STATUS, socket.isConnected)

		socket.on(MESSAGE_SERVICE_STATUS, this.serviceStatusDidUpdate.bind(this))
		this.view.on(MESSAGE_REGISTRATION_REGISTER, this.register.bind(this))
	}

	serviceStatusDidUpdate(){
		this.view.send(MESSAGE_SERVICE_STATUS, socket.isConnected)
	}

	async register(event, code){
		let result = await socket.request(MESSAGE_REGISTRATION_REGISTER, code)

		if(result){
			this.emitter.emit(MESSAGE_REGISTRATION_REGISTERED, result)
		} else {
			this.view.send(MESSAGE_REGISTRATION_ERROR, `Project with code '${code}' not found`)
		}
	}

	on(){
		this.emitter.on(...arguments)
	}

}
