import { app } 			from 'electron'
import socket 			from './ServiceSocket'
import Environment 		from './views/Environment'
import Registration 	from './views/Registration'
import Project 			from './views/Project'
import { log } 			from '../../../common/utils'
import { 
	STORE_ID,
	STORE_PROJECT_SETTINGS,
	MESSAGE_SOCKET_REMOTE_ID,
	MESSAGE_SOCKET_CONNECT,
	MESSAGE_SOCKET_DISCONNECT,
	MESSAGE_SERVICE_STATUS,
	MESSAGE_REGISTRATION_REGISTERED,
	MESSAGE_COMMAND_RESTART,
	MESSAGE_COMMAND_SHUTDOWN
} 						from '../../../common/constants'
import { 
	ProjectSettingsError,
	EnvironmentSettingsError,
	ProjectSetupError
} 						from '../../../common/errors'
import Store 			from './Store'

/**
 * Remote is the main class which controls the flow of information, i.e. decides what to display to the 
 * user, handles information sending and receiving from the service, etc
 */
class Remote {

	constructor(view){
		this.view = view

		this.environment 	= new Environment(this.view)
		this.registration 	= new Registration(this.view)	
		this.project 		= new Project(this.view)
	}

	async init(){

		// move socket messages to separate handlers?
		socket.on(MESSAGE_SOCKET_CONNECT, ()=>{
			log(`Remote connected to Service socket`)
			socket.emit(MESSAGE_SERVICE_STATUS)
		})

		socket.on(MESSAGE_SOCKET_DISCONNECT, ()=>{
			log(`Remote disconnected from Service socket`)
			socket.emit(MESSAGE_SERVICE_STATUS)
		})

		socket.on(MESSAGE_COMMAND_RESTART, async ()=>{
			this.project.stop()
			app.relaunch()
			app.quit() // quit is graceful, compared to exit
		})

		socket.on(MESSAGE_COMMAND_SHUTDOWN, async ()=>{
			this.project.stop()
			app.quit()
		})

		socket.connect()

		try {
			this.project.start()
		} catch(e){

			if(e instanceof ProjectSettingsError){

				// show the Registration window
				await this.registration.loadView()

				this.registration.on(MESSAGE_REGISTRATION_REGISTERED, async settings=>{
					this.project.settings = settings
					this.init()
				})

			} else if(e instanceof ProjectSetupError){

				await this.environment.loadView()
				await this.environment.setup(this.project.settings)
				this.init()
	
			} else {
				// pass up the error
				throw e
			}
		}
	}

	async reset(){
		// delete the project settings
		await this.project.reset()
		app.relaunch()
		app.quit()
	}

	close(){
		this.project.stop()
	}

}

export default Remote
