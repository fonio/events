import { app }	from 'electron'
import path 	from 'path'
import url 		from 'url'
import { 
	MESSAGE_WINDOW_READY
} 				from '../../../../common/constants'

const ViewLoader = target=>{

	return class extends target {

		get parcelDistPath(){
			return `${app.isPackaged ? '../..' : '../../..'}/dist/ui`
		}

		async loadView(){
			return new Promise((resolve, reject)=>{
				if(this.loadViewDelay === undefined || !this.loadViewDelay){
					let pathname = path.resolve(__dirname, this.parcelDistPath, `${this.name}.html`)

					this.view.window.loadURL(url.format({
						pathname,
						protocol: 'file:',
						slashes: true
					}))

					this.view.on(MESSAGE_WINDOW_READY, ()=>{
						if(this.onViewReady){
							this.onViewReady()
						}

						resolve()
					})

					setTimeout(()=>reject(`No response from ${pathname} MESSAGE_WINDOW_READY event`), 3000)
				} else {
					// loadView is delayed, just resolve
					resolve()
				}
			})
		}

	}

}

export default ViewLoader
