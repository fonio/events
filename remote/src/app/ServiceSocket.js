import WebSocket 		from 'ws'
import uuid 			from 'uuid/v4'
import Store 			from './Store'
import Socket			from '../../../common/Socket'
import { 
	SOCKET_TYPE_REMOTE,
	STORE_ID,
	MESSAGE_SOCKET_REMOTE_ID,
} 						from '../../../common/constants'

class ServiceSocket extends Socket {

	constructor(){
		super(SOCKET_TYPE_REMOTE)
		this.isConnected = false
	}

	connect(){
		if(!this.isConnected){
			super.connect(WebSocket, {
				perMessageDeflate: false
			})

			this.socket.on('open', ()=>{
				if(!Store.get(STORE_ID)){
					Store.set(STORE_ID, uuid())
				}

				let id = Store.get(STORE_ID)
				this.send(MESSAGE_SOCKET_REMOTE_ID, id)
				
				this.isConnected = true
			})
		}
	}

}

// only 1 socket object required for the Remote app
export default new ServiceSocket
