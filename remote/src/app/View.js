import EventEmitter from 'events'
import { 
	BrowserWindow, 
	ipcMain,
} 					from 'electron'
import { 
	isEnvDev, 
} 					from '../../../common/utils'

export default class View {

	constructor(){
		this.window = new BrowserWindow({ 
				x: 0, 
				y: 0, 
				width: 800, 
				height: 500, 
				kiosk: false, 
				frame: true, 
				show: false, 
				webPreferences: { 
					nodeIntegration: true
				}
			})

		this.window.webContents.once('dom-ready', ()=>{
			this.window.show()
		})

		if(isEnvDev()){
			this.window.openDevTools()
		}
	}

	send(){
		if(!this.window.isDestroyed()){
			this.window.webContents.send(...arguments)
		}
	}

	on(){
		ipcMain.on(...arguments)
	}

}
