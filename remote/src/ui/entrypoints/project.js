import React, { useState, useEffect } 
						from 'react'
import ReactDOM 		from 'react-dom'
import useViewMessage 	from '../hooks/useViewMessage'
import { 
	MESSAGE_WINDOW_READY,
	MESSAGE_WINDOW_MESSAGE
} 						from '../../../../common/constants'
import { log } 			from '../../../../common/utils'

import styles 			from '../styles/entrypoints/project'

const Project = props=>{

	let [messages, setMessages] = useState([]);
	let [on, send] = useViewMessage()

	useEffect(()=>{
		on(MESSAGE_WINDOW_MESSAGE, (event, message)=>{
			setMessages(messages=>[...messages, message])
		})

		send(MESSAGE_WINDOW_READY)
	}, []);

	return (
		<div>
			<h1>Project</h1>
			<p>Need to do something with this page. Is displayed when a project does not
			have a launch url specified. Logical thing would be to write out logging from the
			projects node process.</p>
			<ul>{messages.map((message, key)=>(
				<li key={key}>{message}</li>
			))}</ul>
		</div>
	)
}

ReactDOM.render(<Project />, document.getElementById('project'))
