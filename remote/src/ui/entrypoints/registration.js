import React, { useState, useEffect, useRef } 
						from 'react'
import ReactDOM 		from 'react-dom'
import useViewMessage 	from '../hooks/useViewMessage'
import { 
	MESSAGE_WINDOW_READY,
	MESSAGE_SERVICE_STATUS,
	MESSAGE_REGISTRATION_REGISTER,
	MESSAGE_REGISTRATION_ERROR
} 						from '../../../../common/constants'

import styles 			from '../styles/entrypoints/registration'

const Registration = props=>{

	let [isEmptyRegCode, setIsEmptyRegCode] = useState(true);
	let [isConnected, setIsConnected] = useState(false);
	let [on, send] = useViewMessage()

	useEffect(()=>{
		// receive updates about service status
		on(MESSAGE_SERVICE_STATUS, (event, isConnected)=>{
			setIsConnected(isConnected);
		})

		// receive registration
		on(MESSAGE_REGISTRATION_REGISTER, (event, msg)=>{
			console.log(msg);
		})

		// receive registration
		on(MESSAGE_REGISTRATION_ERROR, (event, msg)=>{
			console.log(msg);
		})

		// signal window is ready to display
		send(MESSAGE_WINDOW_READY)
	}, []);

	let regCodeInputRef = useRef(),
		register 		= ()=>send(MESSAGE_REGISTRATION_REGISTER, regCodeInputRef.current.value),
		regCodeOnInput 	= ()=>setIsEmptyRegCode(regCodeInputRef.current.value.length === 0)

	return (
		<div>
			<h1>Registration</h1>
			<input ref={regCodeInputRef} onInput={regCodeOnInput} disabled={!isConnected} type="text" name="registration_code" />
			<button disabled={!isConnected || isEmptyRegCode} onClick={register}>Register</button>
			<p>{isConnected ? 'App is connected to service' : 'App is not connected to service'}</p>
		</div>
	)
}


ReactDOM.render(<Registration />, document.getElementById('registration'))

