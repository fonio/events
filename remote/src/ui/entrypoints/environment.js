import React, { useState, useEffect } 
						from 'react'
import ReactDOM 		from 'react-dom'
import useViewMessage 	from '../hooks/useViewMessage'
import { 
	MESSAGE_WINDOW_READY,
	MESSAGE_WINDOW_MESSAGE
} 						from '../../../../common/constants'
import { log } 			from '../../../../common/utils'

import styles 			from '../styles/entrypoints/environment'

const Environment = props=>{

	let [messages, setMessages] = useState([]);
	let [on, send] = useViewMessage()

	useEffect(()=>{
		on(MESSAGE_WINDOW_MESSAGE, (event, message)=>{
			setMessages(messages=>[...messages, message])
		})

		send(MESSAGE_WINDOW_READY)
	}, []);

	return (
		<div>
			<h1>Environment</h1>
			<ul>{messages.map((message, key)=>(
				<li key={key}>{message}</li>
			))}</ul>
		</div>
	)
}

ReactDOM.render(<Environment />, document.getElementById('environment'))
