const { ipcRenderer } = window.require('electron');

export default function useViewMessage() {

	let on = (type, func)=>{
			ipcRenderer.on(type, function(){
				func(...arguments)
			})
		}, 
		send = (type, message)=>{
			ipcRenderer.send(type, message)
		}

	return [ on, send ]
}	
