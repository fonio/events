require('@babel/register')
require('@babel/polyfill')

exports['service:dev'] 	= require('./tasks/service/dev').default
exports['remote:dev'] 	= require('./tasks/remote/dev').default
exports['remote:build'] = require('./tasks/remote/build').default
