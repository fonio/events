# Events Project

This project provides remote deployment and startup of git hosted node projects based on settings supplied by a central service. The aim of the project is to provide easy setup of booth machines to the person onsite at an exhibition/event and then easy management of those machines from a central location. 

This project has 2 distinct parts, one being the remote application ("Remote"), the other being the central service ("Service"). Remote is an Electron application that allows the onsite user to connect to the Service. Once connected, Remote can register and provide Service with a code which corresponds to a git repository and other associated settings. These settings are returned to Remote, which then runs a check on the system to see if it has the necessary software to run (primarily git and node). If the system is not set up, Remote will install what is necessary through a .sh (Mac) or .bat (Windows) script file. Once complete, the repository is cloned and npm install && npm start runs on the cloned project. Remote then provides an interface to that project, which can either be a local url if a webserver has been started, or a view to display the output from any logging written by the process if not.

## Technology Used

* **Node**: platform for running JavaScript server-side. Fundamental to the Events project.

* **Gulp**: a task runner. This runs a pre-programmed list of commands to make managing the project easier, e.g. setting up and starting the development environment.

* **Electron**:	framework to build native applications using web technologies. The Remote application is built on top of this.

* **React**: library to render user interfaces. Both front ends (user interfaces) of Remote and Service are written using this.

* **Parcel**: web application bundler. Both the user interfaces of Remote and Service are bundled using Parcel. The (almost) zero configuration of this tool makes compilation of React projects extremely easy.
	
* **Mongodb**: a javascript object based database. Service uses a mongodb to store each of the project settings. 

* **Websockets**: communications protocol for persistent, 2 way connections between servers and browsers. Provides the communications methods between Remotes and Service(s).

## Installation

Run a git clone on the repo, followed by an npm install on the project.

Mongodb is a requirement for Service, the easiest way to install this on a Mac is to use Homebrew:

    brew install mongodb

If you do not have Homebrew installed, follow the installation instructions at [Homebrew](https://brew.sh/). Mongodb requires a directory to store all its database files in, the default is instructed to be at /data/db from the Mongodb installation guide:

    mkdir -p /data/db
    sudo chown -R `id -un` /data/db
    mongod

However, it appears that this may have changed. If you find that this is the case, run the following command to create the data/db directory in your home directory, then start Mongodb telling it where to store its db files:

    mkdir -p ~/data/db
    mongod --dbpath ~/data/db

If you have not previously installed gulp, you will need the gulp-cli added globally to your environment. Run the following command to do this:

    npm install --global gulp-cli 

## Project Structure

Upon cloning, you will find the following directory structure has been created:

* **common** - stores all files that are common to both Remote and Service.

* **remote** - files that are associated with the Remote application are stored here.

* **service** - all files that are associated with the Service are saved to this location.

* **tasks** - all gulp tasks are located here. These have been separated into Remote and Service specific tasks.

You will find all code for both sub-projects are contained within the src directories of remote and service: 

### remote/src

* **app** - contains code required for the Remote application functionality. MainWindow.js handles the setting up of the Electron window, Remote.js is the starting point of our Remote application. Each chunk of functionality is separated into a "view", each one acts as the interface for the corresponding ui entrypoint.
		
* **ui** - contains all user interface code for the Remote application. Each "view" (Registration, Environment, etc) requires an html container page and an entrypoint js file. Currently, all React code is placed within the entrypoint file, going forward it makes sense to split into components and reference them from the appropriate directory.

* **main.js** - is the main entry point to the application. Checks whether app is packaged or not and runs the appropriate code for development purposes.

* **remote.js** - the entry point to the unpackaged application (loaded from main.js).

### service/src

* **app** - contains code required for all Service functionality.

* **models** - defines Mongoose schemas and models for mongodb collections.

* **routes** - provides routes required for ui interaction.  

* **ui** - contains all files associated with the front end of Service. Built using React, the app is split into components, each with an associated Sass stylesheet. 

* **www** - output from Parcel bundle task. Compiles an html starting point and compiles all referred resources.

## Gulp Tasks

As mentioned, tasks have been split into Service and Remote specific tasks. All tasks have to be exported from the root/gulpfile.js file.

    gulp remote:dev

Sets up and starts the Electron process for Remote application development. Initially will run Parcel to bundle up the user interfaces. These are output and referenced from remote/dist. 

    gulp remote:build

Builds out the Remote application so that it can be distributed to Windows or Mac platforms. Will bundle the user interfaces prior to running, also bundles all es6 code so that it can run within the packaged Electron application.

    gulp service:dev

Starts up the development environment for Service. To faciliate hot reloading of the front end, a Browsersync server acts as proxy to the Service server and a watch tasks monitors all user interface files. The watch task then notifies Browsersync to reload if a change is detected. The Browsersync server port will always be +1 to the SERVICE\_SERVER\_PORT constant set in common/constants.js

## Development Setup

As the project has 2 distinct parts, personally I have found it useful to split my dev environment screen real estate up into the following sections:

    |-----------|
    | 1 | 2 | 3 |
    |-----------|
    |  4  |  5  |
    |-----------|

Where

* 1: The running Remote app.
* 2: The Service front end.
* 3: Tabbed terminal window running 3 tabs, Remote logging, Service logging and Mongodb output.
* 4: Sublime window showing Remote and common directories.
* 5: Sublime window showing Service and common directories.

This is obviously personal preference (feel free to use IDE of choice too). However, I found that splitting the 2 IDE windows in this way compartmentalised the 2 separate areas of functionality in my mind. A useful tool to handle positioning of program windows in a Mac environment is [Spectacle](https://www.spectacleapp.com/).

## Building Remote Application
	
To build the Remote Electron application, issue the gulp remote:build task as detailed above. This runs the Electron builder process and outputs the resulting packages to remote/build. The 2 files that are of concern in that directory are the .dmg and the .exe to be deployed to Mac and Windows platforms respectively, however you can find an unpackaged Mac app in remote/build/mac. 

All configuration settings for the builder are located in package.json under the "build" property (further reading on this topic may be required, it is a subject out of scope of this project). 

## Debugging a built Remote Application

To check logging from a built Remote application, make use of the Macs in-built debugger called lldb. If you are running from the project root, (providing you have successfully built the app) type
    
    lldb remote/build/mac/Remote.app/

This configures the debugger for the provided app. Type ```run``` to start the process. Type ```exit``` to quit once the application window has been closed.

## Existing Issues

* If the Windows install bat file is cancelled or fails halfway through the procedure, it will continuously loop back through the whole process.
* If a 'reset' is performed, or some other internal shutting down and restarting of the Remote app occurs, the app window becomes detached with any logging as it is now running on a separate thread. Not an error as such, just something to keep in mind.

## Intended Project Functionality

### General
* Labelling of machines based on the event they are located at.
* Issuing of update command from Service to Remote (git pull && npm install && npm start). Required if host project is updated.

### Service
* Showing which remote machine has which project installed.

### Remote
* Handle the Project view correctly if no launch url specified, i.e. display the output from the console of the started project node process to the user.
* Ability to set a node version for a project
