import socket from '../js/ServiceSocket' 

export default function useSocket() {

	let on = (type, func)=>{
			socket.on(type, func)
		}, 
		once = (type, func)=>{
			socket.once(type, func)
		}, 
		send = (type, message)=>{
			socket.send(type, message)
		},
		request = async (type, message)=>{
			return await socket.request(type, message)
		}

	return [ on, once, send, request ]
}	
