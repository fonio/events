import { 
	SOCKET_TYPE_SERVICE,
} 						from '../../../../common/constants'
import Socket 			from '../../../../common/Socket'

class ServiceSocket extends Socket {

	constructor(){
		super(SOCKET_TYPE_SERVICE)
	}

}

// ui requires only one ServiceSocket set up
const socket = new ServiceSocket()
socket.connect()

export default socket

