import React 		from 'react'
import ReactDOM 	from 'react-dom'
import Service 		from '../components/Service' 

(async ()=>{
	ReactDOM.render(<Service />, document.getElementById('service'))
})()
