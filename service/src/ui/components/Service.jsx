import React, { 
	useState, useEffect 
} 							from 'react'
import ConnectedRemotes 	from './ConnectedRemotes'
import Projects 			from './Projects'
import useSocket 			from '../hooks/useSocket'
import { 
	MESSAGE_SOCKET_CONNECT,
	MESSAGE_SOCKET_ERROR,
	MESSAGE_SOCKET_DISCONNECT,
} 							from '../../../../common/constants'

import styles from '../styles/Service'

const Service = props=>{

	let [isConnected, setIsConnected] = useState(false)
	let [once, send] = useSocket()

	useEffect(()=>{

		once(MESSAGE_SOCKET_CONNECT, ()=>{
			setIsConnected(true)
		})

		once(MESSAGE_SOCKET_DISCONNECT, ()=>{
			setIsConnected(false)
		})

	}, []);

	return (
		<div>
			<span className={[styles.connection, isConnected ? styles.connected : styles.disconnected].join(' ')}>{isConnected ? 'connected' : 'no connection'}</span>
			<h1>Service</h1>
			<ConnectedRemotes />
			<Projects />
		</div>
	)
}

export default Service
