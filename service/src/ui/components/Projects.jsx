import React, { 
	useState, 
	useEffect,
	useRef
} 							from 'react'

import Container 	from "react-bootstrap/Container"
import Table 		from "react-bootstrap/Table"
import Form 		from "react-bootstrap/Form"
import Button 		from "react-bootstrap/Button"

import styles from '../styles/Projects'

const Projects = props=>{

	let formEl							= useRef(null)
	let [projects, setProjects] 		= useState([])
	let [validated, setValidated] 		= useState(false);
	let [editFormProject, setEditFormProject] 	= useState(null);

	useEffect(()=>{
		requestProjects()
	}, []);

	// methods
	let requestProjects = async ()=>{
		let res = await fetch('/projects'),
			projects = await res.json()

		setProjects(projects)
	}

	let handleDeleteProject = async id=>{
		let res = await fetch('/projects', {
			body: JSON.stringify({ id }),
			headers: { 'content-type': 'application/json' },
			method: 'DELETE'
		})

		requestProjects()		
	}

	let handleFormReset = async ()=>{
		setEditFormProject(null)
		formEl.current.reset()
	}

	let handleEditProject = async _id=>{
		let project = projects.filter(project=>project._id === _id)[0]

		setEditFormProject(project)

		Array.prototype.slice.call(formEl.current).map(input=>{
			if(project[input.name]){
				input.value = project[input.name]
			}

			if(input.name === 'id'){
				input.value = project._id
			}
		})
	}

	let handleSubmit = async event=>{
		event.preventDefault();
		event.stopPropagation();

		let form = formEl.current;

		setValidated(true);
			
		if(form.checkValidity()){
			let fields = Array.prototype.slice.call(form).reduce((acc, input)=>{
				if(input.name && input.value){
					acc[input.name] = input.value
				}
				return acc
			}, {})

			let res = await fetch('/projects', {
				body: JSON.stringify(fields),
				headers: {'content-type': 'application/json'},
				method: fields.id ? 'PUT' : 'POST'
			})
			
			let success = await res.json()
			setValidated(false);
			form.reset()
			setEditFormProject(null)

			requestProjects()
		} 
	}

	return (
		<Container>
			<Form noValidate validated={validated} onSubmit={handleSubmit} ref={formEl}>
				<h2>Projects</h2>
				<Table className={styles.projectsTable}>
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Registration Code</th>
							<th>Repository</th>
							<th>Launch URL</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{projects.map((project, index)=>(
							<tr key={index}>
								<td>{index + 1}</td>
								<td>{project.name}</td>
								<td>{project.registration_code}</td>
								<td className={styles.repositoryUrl}>{project.repository_url}</td>
								<td className={styles.launchUrl}>{project.launch_url}</td>
								<td>
									<Button variant="secondary" type="button" onClick={()=>handleEditProject(project._id)}>Edit</Button>
									<Button variant="danger" type="button" onClick={()=>handleDeleteProject(project._id)}>Delete</Button>
								</td>
							</tr>
						))}	
						<tr>
							<td><Form.Control type="hidden" name="id" /></td>
							<td><Form.Control required minLength="4" type="text" name="name" /></td>
							<td><Form.Control required minLength="4" type="text" name="registration_code" /></td>
							<td><Form.Control required type="text" name="repository_url" /></td>
							<td><Form.Control type="text" name="launch_url" /></td>
							<td>
								<Button variant="secondary" onClick={()=>handleFormReset()}>Reset</Button>
								<Button variant="primary" type="submit" className={[styles.submitBtn, editFormProject ? styles.edit : styles.add].join(' ')}></Button>
							</td>
						</tr>
					</tbody>
				</Table>
			</Form>
		</Container>
	)
}

export default Projects
