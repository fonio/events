import React, { useState, useEffect } from 'react'

// bootstrap stuff
import Container 	from "react-bootstrap/Container";
import Table 		from "react-bootstrap/Table";
import Button 		from "react-bootstrap/Button"

import useSocket 	from '../hooks/useSocket'
import { 
	MESSAGE_SOCKET_CONNECT,
	MESSAGE_SOCKET_ERROR,
	MESSAGE_SOCKET_DISCONNECT,
	MESSAGE_CONNECTED_REMOTES,
	MESSAGE_CONNECTED_REMOTES_UPDATED,
	MESSAGE_COMMAND_RESTART,
	MESSAGE_COMMAND_SHUTDOWN,
} 					from '../../../../common/constants'

import styles from '../styles/ConnectedRemotes'

const ConnectedRemotes = props=>{

	let [count, setCount] = useState(0)
	let [remotes, setRemotes] = useState([])
	let [on, once, send, request] = useSocket()

	useEffect(()=>{

		on(MESSAGE_SOCKET_CONNECT, async ()=>{
			let remotes = await request(MESSAGE_CONNECTED_REMOTES)
			setRemotes(remotes)
		})

		on(MESSAGE_CONNECTED_REMOTES_UPDATED, remotes=>{
			setRemotes(remotes)
		})	

		on(MESSAGE_SOCKET_DISCONNECT, ()=>{
			setRemotes([])
		})

	}, []);

	let command = async (command, remoteId)=>{
		let result = await request(command, remoteId)
	}

	return (
		<Container>
			<h2>Remotes</h2>
			<Table className={styles.remotesTable}>
				<thead>
					<tr>
						<th>#</th>
						<th>id</th>
						<th>Send commands</th>
					</tr>
				</thead>
				<tbody>
					{remotes.map((remote, index)=>(
						<tr key={index}>
							<td>{index + 1}</td>
							<td>{remote.id}</td>
							<td>
								<Button variant="info" onClick={()=>command(MESSAGE_COMMAND_RESTART, remote.id)}>Restart</Button>
								<Button variant="info" onClick={()=>command(MESSAGE_COMMAND_SHUTDOWN, remote.id)}>Shutdown</Button>
							</td>
						</tr>
					))}	
				</tbody>
			</Table>
		</Container>
	)
}

export default ConnectedRemotes
