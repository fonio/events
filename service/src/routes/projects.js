import express from 'express'
import Project from '../models/Project'

const router = express.Router()

router.route('/projects')

	.get(async (req, res)=>{
		let projects = await Project.find({}).exec()
		res.send(JSON.stringify(projects))
	})

	.post(async (req, res)=>{
		let { name, registration_code, repository_url } = req.body

		let project = new Project({ name, registration_code, repository_url })
		await project.save()

		res.send(project)
	})

	.put(async (req, res)=>{

		let { name, registration_code, repository_url, launch_url, id } = req.body

		let project = await Project.findOne({ _id: id }).exec()

		project.name 				= name
		project.registration_code 	= registration_code
		project.repository_url 		= repository_url
		project.launch_url 			= launch_url
		
		await project.save()

		res.send(project)
	})

	.delete(async (req, res)=>{
		let success = await Project.deleteOne({ _id: req.body.id }).exec()
		res.send(success)
	})

export default router
