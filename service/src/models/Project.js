import mongoose from 'mongoose'

const ProjectSchema = new mongoose.Schema({
	name: { 
		type: String,
		required: true
	},
	registration_code: { 
		type: String,
		required: true
	},
	repository_url: {
		type: String,
		required: true
	},
	launch_url: {
		type: String
	},
});

export default mongoose.model('Project', ProjectSchema);
