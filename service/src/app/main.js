import Sockets 			from './Sockets'
import Server 			from './Server'
import Service 			from './Service'
import db 				from './db'

(async ()=>{

	let server 	= new Server,
		sockets = new Sockets

	new Service(sockets, server)

	await db.connect()
	await server.start()
	await sockets.start()

})()
