import mongoose 	from 'mongoose'
import EventEmitter from 'events'
import {
	SERVICE_URI,
	SERVICE_SERVER_PORT,
	SERVICE_SOCKETS_PORT,
	MESSAGE_SERVER_STARTED,
	MESSAGE_SERVER_STOPPED,
}					from '../../../common/constants'

mongoose.connect('mongodb://localhost/service', { 
	useUnifiedTopology: true,
	useNewUrlParser: 	true,
	connectTimeoutMS: 	5000,
});

mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

export default {

	connect: ()=>{
		return new Promise((resolve, reject)=>{
			mongoose.connection.once('connected', resolve);
		})
	}

}
