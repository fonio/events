import CommandHandler 		from './handlers/CommandHandler'
import RegistrationHandler 	from './handlers/RegistrationHandler'
import {
	MESSAGE_SOCKET_READY,
	MESSAGE_SERVER_STARTED,
	MESSAGE_REGISTRATION_REGISTER,
	MESSAGE_SOCKET_CONNECT,
	MESSAGE_SOCKET_BEFORE_DISCONNECT,
	MESSAGE_SOCKET_DISCONNECT,
	MESSAGE_SOCKET_TYPE,
	MESSAGE_SOCKET_REMOTE_ID,
	SOCKET_TYPE_REMOTE,
	SOCKET_TYPE_SERVICE,
	MESSAGE_CONNECTED_REMOTES,
	MESSAGE_CONNECTED_REMOTES_UPDATED,
	MESSAGE_COMMAND,
} 							from '../../../common/constants'

class Service {

	constructor(sockets, server){

		let remotes = [],
			services = []

		server.on(MESSAGE_SERVER_STARTED, ()=>{
			console.log(`Socket server started on ${server.address}`);
		})

		sockets.on(MESSAGE_SOCKET_READY, ()=>{
			console.log('Socket server ready');
		})

		sockets.on(MESSAGE_SOCKET_CONNECT, socket=>{
			console.log('Socket connect');

			socket.on(MESSAGE_SOCKET_TYPE, type=>{
				socket.type = type

				switch(type){
					case SOCKET_TYPE_SERVICE:
						services.push(socket)
						break
				}

				// tell all service frontends that a new remote has connected
				sockets.broadcast(MESSAGE_CONNECTED_REMOTES_UPDATED, remotes, SOCKET_TYPE_SERVICE)
			})

			socket.on(MESSAGE_SOCKET_REMOTE_ID, id=>{
				socket.id = id
				remotes.push(socket)
				sockets.broadcast(MESSAGE_CONNECTED_REMOTES_UPDATED, remotes, SOCKET_TYPE_SERVICE)
			})

			socket.on(MESSAGE_CONNECTED_REMOTES, ()=>{
				socket.send(MESSAGE_CONNECTED_REMOTES, remotes)
			})

			socket.on(MESSAGE_COMMAND, msg=>{
				console.log(`Message from ${socket.id}: ${msg}`);
			})

			// socket handlers
			new RegistrationHandler(socket)
			new CommandHandler(socket)

		})

		// Socket has disconnected, remove from the pool
		sockets.on(MESSAGE_SOCKET_DISCONNECT, id=>{
			console.log(`Socket disconnect ${id}`);

			remotes = remotes.filter(remote=>remote.id !== id)
			services = services.filter(service=>service.id !== id)

			sockets.broadcast(MESSAGE_CONNECTED_REMOTES_UPDATED, remotes, SOCKET_TYPE_SERVICE)
		})

	}

}

export default Service

