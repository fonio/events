import WebSocket 	from 'ws'
import EventEmitter from 'events'
import uuid 		from 'uuid/v4'
import Socket 		from '../../../common/Socket'
import {
	SERVICE_SOCKETS_PORT,
	MESSAGE_SOCKET_READY,
	MESSAGE_SOCKET_CONNECT,
	MESSAGE_SOCKET_DISCONNECT,
} 					from '../../../common/constants'

class Sockets extends EventEmitter {

	constructor(){
		super()
		this.sockets = []
	}

	start(){
		return new Promise((resolve, reject)=>{
			this.wss = new WebSocket.Server({
				port: SERVICE_SOCKETS_PORT,
				noServer: true
			}, ()=>{
				this.wss.on('listening', ()=>this.emit(MESSAGE_SOCKET_READY))
				this.wss.on('connection', socket=>{

					// add socket to the list
					this.sockets.push(socket)

					socket.on('message', data=>{
						let { type, message } = JSON.parse(data)
						socket.emit(type, message);
					})

					// append functionality to the WebSocket
					socket.send = (type, message, toRemoteId)=>{
						if(toRemoteId){
							// find the socket with the supplied id and pass on the message
							this.sockets.find(socket=>socket.id===toRemoteId).send(type, message)
						} else {
							WebSocket.prototype.send.call(socket, JSON.stringify({ type, message }));
						}
					}

					// append broadcast functionality to socket
					socket.broadcast = ()=>this.broadcast.call(this, ...arguments)

					socket.on('close', ()=>{
						// remove socket from the list
						this.sockets = this.sockets.filter(_socket=>_socket.id !== socket.id)
						this.emit(MESSAGE_SOCKET_DISCONNECT, socket.id)
					})

					// signal this socket is connected and ready
					this.emit(MESSAGE_SOCKET_CONNECT, socket)
				})
				resolve()
			})
		})
	}

	broadcast(type, message, toType=false){
		// filter if a type is provided
		this.sockets.filter(socket=>toType ? toType === socket.type : true).forEach(socket=>{
			if(socket.readyState === 1){
				socket.send(type, message)
			} else {
				// socket is dead for some reason, remove
				this.sockets = this.sockets.filter(_socket=>_socket !== socket)
			}
		})
	}

	close(){
		return new Promise((resolve, reject)=>{
			this.wss.close(resolve)
		})
	}

}

export default Sockets

