import express 		from 'express'
import http 		from 'http'
import EventEmitter from 'events'
import bodyParser 	from 'body-parser'
import {
	SERVICE_URI,
	SERVICE_SERVER_PORT,
	SERVICE_SOCKETS_PORT,
	MESSAGE_SERVER_STARTED,
	MESSAGE_SERVER_STOPPED,
}					from '../../../common/constants'
import projects 	from '../routes/projects'

let app 		= express(),
	httpserver 	= http.Server(app)

class Server extends EventEmitter {

	constructor(){
		super()
		app.use(bodyParser.urlencoded({ extended: true }));
		app.use(bodyParser.json());
		app.use(express.static(`${__dirname}/../www`))
		app.use(projects)
	}

	get address(){
		return `${httpserver.address().address}:${httpserver.address().port}`
	}

	start(){
		httpserver.listen(SERVICE_SERVER_PORT)

		return new Promise((resolve, reject)=>{
			httpserver.on('listening', ()=>{
				this.send(MESSAGE_SERVER_STARTED)
				resolve()
			})
		})
	}

	stop(){
		httpserver.close()

		return new Promise((resolve, reject)=>{
			httpserver.on('close', ()=>{
				this.send(MESSAGE_SERVER_STOPPED)
				resolve()
			})
		})
	}

	/**
	 * Pass get requests to express 
	 * @return {[type]} [description]
	 */
	get(){
		app.get(...arguments)
	}

	send(){
		this.emit(...arguments)
	}

}

export default Server
