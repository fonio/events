import {
	MESSAGE_COMMAND_SHUTDOWN,
	MESSAGE_COMMAND_RESTART,
	SOCKET_TYPE_REMOTE
} 				from '../../../../common/constants'
import { log }	from '../../../../common/utils'

/**
 * Receives a socket, sets up events to listen to and processes them accordingly
 */
export default class CommandHandler {

	constructor(socket){
		this.socket = socket
		this.socket.on(MESSAGE_COMMAND_RESTART, remoteId=>this.issueCommand(MESSAGE_COMMAND_RESTART, remoteId))
		this.socket.on(MESSAGE_COMMAND_SHUTDOWN, remoteId=>this.issueCommand(MESSAGE_COMMAND_SHUTDOWN, remoteId))
	}

	async issueCommand(command, remoteId){
		this.socket.send(command, null, remoteId)	// sends null message
	}

}
