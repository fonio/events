import {
	MESSAGE_REGISTRATION_REGISTER,
} 				from '../../../../common/constants'
import { log }	from '../../../../common/utils'
import Project 	from '../../models/Project'

/**
 * Receives a socket, sets up events to listen to and processes them accordingly
 */
export default class RegistrationHandler {

	constructor(socket){
		this.socket = socket
		this.socket.on(MESSAGE_REGISTRATION_REGISTER, this.register.bind(this))
	}

	async register(registration_code){
		let result = await Project.findOne({ registration_code })
		log(`RegistrationHandler: code '${registration_code}' returns ${result}`)
		this.socket.send(MESSAGE_REGISTRATION_REGISTER, result)
	}

}
