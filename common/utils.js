import console 	from 'chalk-console'
import {
	ENV_PRODUCTION,
	ENV_DEVELOPMENT,
}				from './constants'

/*
	chalk-console supports the following colors:
		red, gray, blue, cyan, white, green, yellow
 */
export const log = (msg, opts)=>{

	let defaults = Object.assign({ color: 'blue' }, opts || {}),
		date = new Date,
		pad = int=>int.toString().padStart(2, '0'),
		capitalise = str=>str.charAt(0).toUpperCase(0) + str.substr(1),
		formattedDate = `${pad(date.getDate())}/${pad(date.getMonth() +1)}/${date.getFullYear().toString().substr(-2)}`,
		formattedTime = `${pad(date.getHours())}:${pad(date.getMinutes())}:${pad(date.getSeconds())}`

	console[defaults.color](`[${formattedDate}@${formattedTime}]
${msg}`);
}

export const isEnvProd = ()=>{
	return process.env.NODE_ENV === ENV_PRODUCTION
}

export const isEnvDev = ()=>{
	return process.env.NODE_ENV === ENV_DEVELOPMENT
}
