import EventsEmitter 	from 'events'
import uuid 			from 'uuid/v4'
import {
	SERVICE_URI,
	SERVICE_SOCKETS_PORT,
	MESSAGE_SOCKET_CONNECT,
	MESSAGE_SOCKET_DISCONNECT,
	MESSAGE_SOCKET_ERROR,
	MESSAGE_SOCKET_TYPE,
	MESSAGE_SOCKET_BROADCAST,

} 						from './constants'
import { log } 			from './utils'

export default class Socket extends EventsEmitter {

	get readyState(){
		return this.socket.readyState
	}

	get socketUri(){
		return `ws://${SERVICE_URI}:${SERVICE_SOCKETS_PORT}`
	}

	constructor(type){
		super()
		this.isConnected 		= false
		this.attemptReconnect 	= true
		this.type = type
	}

	connect(websocketClass=WebSocket, opts){
		this.socket = new websocketClass(this.socketUri, opts)
		this.socket.addEventListener('open', ()=>{
			log(`${this.type} connected`)
			this.isConnected = true
			this.emit(MESSAGE_SOCKET_CONNECT)
			this.send(MESSAGE_SOCKET_TYPE, this.type)
		})

		this.socket.addEventListener('close', ()=>{
			if(this.isConnected){
				log(`${this.type} disconnected`)
				this.isConnected = false
				this.emit(MESSAGE_SOCKET_DISCONNECT)
			}

			setTimeout(()=>this.reconnect(), 3000)			
		})

		this.socket.addEventListener('message', json=>{
			// decode the payload
			let { type, message } = JSON.parse(json.data)

			log(`Received ${this.type} message ${type}: ${message}`, { color: 'gray' })
			this.emit(type, message);
		})

		this.socket.addEventListener('error', reason=>{
			log(`Socket error: ${reason.error}`, { color: 'gray' })
			this.emit(MESSAGE_SOCKET_ERROR)
		})
	}

	reconnect(){
		if(!this.isConnected && this.attemptReconnect){
			log(`${this.type} attempting reconnect to ${this.socketUri}...`)
			this.connect()
		}
	}

	disconnect(){
		this.attemptReconnect = false
		this.socket.close()
	}

	send(type, message){
		if(this.isConnected){
			log(`Sending ${this.type} message ${type}: ${message}`, { color: 'gray' })
			// encode the payload with type and message
			this.socket.send(JSON.stringify({ type, message }))
		}
	}

	async request(type, message){
		return new Promise((resolve, reject)=>{
			this.on(type, resolve)
			this.send(type, message)
		})
	}

}

