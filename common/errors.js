export class ProjectSettingsError extends Error {}
export class EnvironmentSettingsError extends Error {}
export class ProjectSetupError extends Error {}
export class GitNotInstalledError extends Error {}
export class NodeNotInstalledError extends Error {}
export class NodeVersionError extends Error {}
export class NpmInstallNotRunError extends Error {}
