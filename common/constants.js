import os from 'os'

export const ENV_PRODUCTION					= 'ENV_PRODUCTION'
export const ENV_DEVELOPMENT				= 'ENV_DEVELOPMENT'

export const VERSION_NODE 					= '>=12'
export const VERSION_BREW 					= '>=1'
export const VERSION_CHOCO					= '>=0.0.1'

export const SERVICE_URI 					= '10.2.0.159'
export const SERVICE_SERVER_PORT 			= 1234
export const SERVICE_SOCKETS_PORT 			= 12345

export const SOCKET_TYPE_REMOTE 			= 'SOCKET_TYPE_REMOTE'
export const SOCKET_TYPE_SERVICE 			= 'SOCKET_TYPE_SERVICE'

export const MESSAGE			 			= 'MESSAGE'	// a generic message type, not advised to use

export const MESSAGE_SOCKET_TYPE 			= 'MESSAGE_SOCKET_TYPE'

export const MESSAGE_SERVER_STARTED 		= 'MESSAGE_SERVER_STARTED'
export const MESSAGE_SERVER_STOPPED 		= 'MESSAGE_SERVER_STOPPED'

export const MESSAGE_SOCKET_READY 			= 'MESSAGE_SOCKET_READY'
export const MESSAGE_SOCKET_CONNECT 		= 'MESSAGE_SOCKET_CONNECT'
export const MESSAGE_SOCKET_DISCONNECT 		= 'MESSAGE_SOCKET_DISCONNECT'
export const MESSAGE_SOCKET_ERROR 			= 'MESSAGE_SOCKET_ERROR'
export const MESSAGE_SOCKET_REMOTE_ID 		= 'MESSAGE_SOCKET_REMOTE_ID'
export const MESSAGE_SOCKET_BROADCAST 		= 'MESSAGE_SOCKET_BROADCAST'

export const MESSAGE_SERVICE_STATUS 		= 'MESSAGE_SERVICE_STATUS'
export const MESSAGE_SERVICE_ERROR 			= 'MESSAGE_SERVICE_ERROR'

export const MESSAGE_WINDOW_READY 			= 'MESSAGE_WINDOW_READY'
export const MESSAGE_WINDOW_MESSAGE 		= 'MESSAGE_WINDOW_MESSAGE'

export const MESSAGE_REGISTRATION_REGISTER 		= 'MESSAGE_REGISTRATION_REGISTER'
export const MESSAGE_REGISTRATION_REGISTERED 	= 'MESSAGE_REGISTRATION_REGISTERED'
export const MESSAGE_REGISTRATION_ERROR			= 'MESSAGE_REGISTRATION_ERROR'

export const MESSAGE_CONNECTED_REMOTES 			= 'MESSAGE_CONNECTED_REMOTES'
export const MESSAGE_CONNECTED_REMOTES_UPDATED 	= 'MESSAGE_CONNECTED_REMOTES_UPDATED'

export const MESSAGE_COMMAND 				= 'MESSAGE_COMMAND'
export const MESSAGE_COMMAND_RESTART 		= 'MESSAGE_COMMAND_RESTART'
export const MESSAGE_COMMAND_SHUTDOWN 		= 'MESSAGE_COMMAND_SHUTDOWN'

export const MESSAGE_COMMAND_DATA			= 'MESSAGE_COMMAND_DATA'
export const MESSAGE_COMMAND_ERROR			= 'MESSAGE_COMMAND_ERROR'
export const MESSAGE_COMMAND_COMPLETE		= 'MESSAGE_COMMAND_COMPLETE'

export const STORE_ID 						= 'STORE_ID'
export const STORE_PROJECT_SETTINGS 		= 'STORE_PROJECT_SETTINGS'
export const STORE_ENVIRONMENT_SETTINGS 	= 'STORE_ENVIRONMENT_SETTINGS'

let version_platorm_package_manager
switch(os.platform()){
	case 'darwin': 
		version_platorm_package_manager = VERSION_BREW; break;
	case 'win32': 
		version_platorm_package_manager = VERSION_CHOCO; break;
	case 'browser': 
		version_platorm_package_manager = null; break;	// not applicable
	default: 
		throw new Error(`Unknown os '${os.platform()}' when setting VERSION_PM`)
}

export const VERSION_PM = version_platorm_package_manager

