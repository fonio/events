import gulp 		from 'gulp'
import parcel 		from 'gulp-parcel'
import del 			from 'del'

let bundle = async done=>{
	
	let publicPath = 'service/src/www'

	await del(`${publicPath}/*`)

	gulp.src('service/src/ui/html/*.html', { read: false })
		.pipe(parcel({
			watch: 		false,
			outDir: 	publicPath,
			cacheDir: 	'service/.cache',
		}))
		.pipe(gulp.dest(publicPath).on('end', ()=>done()))
		
}

export default bundle