import gulp 		from 'gulp'
import watch 		from 'gulp-watch'
import nodemon 		from 'gulp-nodemon'
import browsersync 	from 'browser-sync'

import bundle 		from './bundle'
import {
	SERVICE_URI,
	SERVICE_SERVER_PORT
} 					from '../../common/constants'

let runNodemon = async ()=>{

	let nm = nodemon({
		script: `service/src/app/main`,
		exec: 'babel-node',
		ignore: [
			'node_modules', 
			'tasks', 
			'remote', 
			'service/src/www', 
			'service/src/ui', 
			'service/.cache',
			'package*',
		],
		delay: 100
	})

	let bs = new browsersync({
		proxy: {
			target: `http://${SERVICE_URI}:${SERVICE_SERVER_PORT}`,
		},
		open: 	false,
		ui: 	false,
		notify: false,
		port: 	SERVICE_SERVER_PORT +1
	})

	watch(['service/src/ui/**/*'], gulp.series(bundle, done=>{bs.reload(); done()}))

	return Promise.resolve()
}

export default gulp.series(bundle, runNodemon)

