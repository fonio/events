import gulp 				from 'gulp'
import watch 				from 'gulp-watch'
import electronConnect 		from 'electron-connect'

import bundle				from './bundle'
import { ENV_DEVELOPMENT }	from '../../common/constants'
import { 
	setEnvDev
}							from '../common'

let electron = electronConnect.server.create()

const dev = ()=>{
	electron.start(()=>{
		watch([
			'./remote/src/app/**/*.js',
			'./common/**/*.js',
		], electron.restart)
	});

	return Promise.resolve()
}

export default gulp.series(setEnvDev, bundle, dev)
