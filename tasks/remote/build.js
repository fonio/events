import gulp 		from 'gulp'
import del 			from 'del'
import parcel 		from 'gulp-parcel'
import { spawn } 	from 'child_process'
import bundle		from './bundle'
import { 
	setEnvProd
}					from '../common'

let clean = async done=>{
	await del([
		'remote/build',
		'remote/dist',
		'!remote/src',
	])
	done()
}

let bundle_app = done=>{

	let distPath = 'remote/dist/app'

	gulp.src('remote/src/remote.js', { read: false })
		.pipe(parcel({
			target: 	'electron',
			watch: 		false,
			outDir: 	distPath,
			cacheDir: 	'remote/.cache/app',
		}))
		.pipe(gulp.dest(distPath))
		.on('end', done)

}

let build = done=>{

	let npm = spawn('npm', ['run', 'dist'], { cwd: `${__dirname}/../` });

	npm.stdout.on('data', data=>{console.log(data.toString('utf8'));});
	npm.stderr.on('error', error=>{console.log(error.toString('utf8'));});
	npm.on('close', ()=>done());

}

export default gulp.series(setEnvProd, clean, bundle_app, bundle, build)
