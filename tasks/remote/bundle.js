import gulp 		from 'gulp'
import parcel 		from 'gulp-parcel'
import { 
	isEnvProd
}					from '../../common/utils'

let bundle = done=>{
	
	let distPath = 'remote/dist/ui'

	gulp.src('remote/src/ui/html/*.html', { read: false })
		.pipe(parcel({
			target: 	'electron',
			watch: 		!isEnvProd(),
			outDir: 	distPath,
			cacheDir: 	'remote/.cache',
			publicURL: 	'./',
		}))
		.pipe(gulp.dest(distPath))
		.on('end', done)

}

export default bundle