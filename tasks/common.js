import { 
	ENV_PRODUCTION,
	ENV_DEVELOPMENT,
}	from '../common/constants'

export const setEnvProd = done=>{
	process.env.NODE_ENV = ENV_PRODUCTION
	done()
}

export const setEnvDev = done=>{
	process.env.NODE_ENV = ENV_DEVELOPMENT
	done()
}

